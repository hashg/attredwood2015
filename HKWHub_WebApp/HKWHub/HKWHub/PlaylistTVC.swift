//
//  PlaylistTVC.swift
//  fcsample-ios
//
//  Created by Seonman Kim on 12/15/14.
//  Copyright (c) 2014 Seonman Kim. All rights reserved.
//

import UIKit
import MediaPlayer
import AVFoundation
import CoreData




var g_playList : [Playlist] = [Playlist]()


func loadPlaylistItems() {
    
    let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
    let context: NSManagedObjectContext = appDel.managedObjectContext!
    
    let req = NSFetchRequest(entityName: "Playlist")
    
    g_playList = context.executeFetchRequest(req, error: nil)! as! [Playlist]
    
    for item in g_playList {
        configureItem(item)
    }
}

func configureItem(playList: Playlist) {
    // Configure the cell...
    var data: NSManagedObject = playList as NSManagedObject
    let mediaItemPersistentID = data.valueForKey("item_persistentID") as? String
    
    // query the media library
    let query = MPMediaQuery.songsQuery()
    
    let predicate = MPMediaPropertyPredicate(value: mediaItemPersistentID,
        forProperty: MPMediaItemPropertyPersistentID)
    query.addFilterPredicate(predicate)
    if let item = query.items.first as? MPMediaItem {
        playList.mediaItem = item
        
        playList.item_url = item.assetURL.absoluteString
        if playList.item_url == nil {
            playList.item_url = ""
        }
        
        playList.item_title = item.title
        if playList.item_title == nil {
            playList.item_title = "Unknown"
        }
        
        playList.item_artist = item.artist
        if playList.item_artist == nil {
            playList.item_artist = "Unknown"
        }
        
        playList.item_album_title = item.albumTitle
        if playList.item_album_title == nil {
            playList.item_album_title = "Unknown"
        }
        
        playList.item_duration = item.playbackDuration
        
        var itemArtwork = item.artwork
        if let artwork = itemArtwork {
            playList.artworkImageSmall = artwork.imageWithSize(CGSizeMake(60.0, 60.0))
            playList.artworkImageBig = artwork.imageWithSize(CGSizeMake(240.0, 240.0))
        } else {
            playList.artworkImageSmall = UIImage(named: "ic_audiotrack_black_48dp")
            playList.artworkImageBig = UIImage(named: "ic_audiotrack_black_48dp")
        }
    } else {
        println("configureItem: mediaItem is nil")
    }
}


class PlaylistTVC: UITableViewController, MPMediaPickerControllerDelegate, AVAudioPlayerDelegate {
    
    var itemList: NSMutableArray!
    

    var mediaPicker: MPMediaPickerController?
    var launchMediaPicker = false
    
    var warningInAddingSongTitle = false
    var warningMesg: String!

    @IBOutlet var btnNowPlaying: UIBarButtonItem!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if launchMediaPicker {
            displayMediaPickerAndPlayItem()
            return
        }
        
        if g_selectedIndex >= 0 {
            btnNowPlaying.enabled = true
        }

//        HKWControlHandler.debugPrintOn(true)
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)

        if g_playList.count == 0 {
            loadPlaylistItems()
        }
        
        tableView.reloadData()
    }
    

    @IBAction func showMenu(sender: AnyObject) {
        // Dismiss keyboard (optional)
        self.view.endEditing(true )
        self.frostedViewController.view.endEditing(true )
        
        // Present the view controller
        self.frostedViewController.presentMenuViewController()
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source

    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return g_playList.count
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCellWithIdentifier("Title_Cell", forIndexPath: indexPath) as! PlaylistTableViewCell

        cell.titleLabel?.text = g_playList[indexPath.row].item_title
        cell.artistLabel?.text = g_playList[indexPath.row].item_artist
        cell.albumLabel?.text = g_playList[indexPath.row].item_album_title
        let duration = Int(g_playList[indexPath.row].item_duration)
        var min = duration / 60
        var sec = duration % 60
        var formatStr: String = String(format: "%d:%02d", min, sec)
        println("duration: \(formatStr)")
        cell.durationLabel?.text = formatStr
        cell.albumArtImageView.image = g_playList[indexPath.row].artworkImageSmall

        return cell
    }

    // Override to support conditional editing of the table view.
    override func tableView(tableView: UITableView, canEditRowAtIndexPath indexPath: NSIndexPath) -> Bool {
        // Return NO if you do not want the specified item to be editable.
        return true
    }

    // Override to support editing the table view.
    override func tableView(tableView: UITableView, commitEditingStyle editingStyle: UITableViewCellEditingStyle, forRowAtIndexPath indexPath: NSIndexPath) {
        let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
        let context: NSManagedObjectContext = appDel.managedObjectContext!
        
        if editingStyle == .Delete {
            //    context.deletedObject(myList[indexPath!.row] as NSManagedObject)
            context.deleteObject(g_playList[indexPath.row] as NSManagedObject)
            g_playList.removeAtIndex(indexPath.row)
            tableView.deleteRowsAtIndexPaths([indexPath], withRowAnimation: UITableViewRowAnimation.Fade)
            
            
            var error: NSError? = nil
            if !context.save(&error) {
                abort()
            }

        }
    }


    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using [segue destinationViewController].
        // Pass the selected object to the new view controller.
    }

    // MARK: - Media Picker
    func displayMediaPickerAndPlayItem(){
        
        mediaPicker = MPMediaPickerController(mediaTypes: .AnyAudio)
        
        if let picker = mediaPicker {
            
            println("Successfully instantiated a media picker")
            picker.delegate = self
            picker.allowsPickingMultipleItems = true
            picker.showsCloudItems = true
            picker.prompt = "Pick songs to add"
            view.addSubview(picker.view)
            
            presentViewController(picker, animated: true, completion: nil)
            
        } else {
            println("Could not instantiate a media picker")
        }
        
    }

    func mediaPicker(mediaPicker: MPMediaPickerController!, didPickMediaItems mediaItemCollection: MPMediaItemCollection!) {
        for thisItem in mediaItemCollection.items as! [MPMediaItem] {
            let isCloudItem = thisItem.valueForProperty(MPMediaItemPropertyIsCloudItem) as! Bool
            if isCloudItem {
                println("this music item is in the cloud")
                let itemTitle = thisItem.valueForProperty(MPMediaItemPropertyTitle) as? String
                if let title = itemTitle {
                    warningMesg = "'\(title)' has not been added to the PlayList, because it is not on the device (The song is in iTunes Match)."
                }
                warningInAddingSongTitle = true
                println("mesg: \(warningMesg)")
                continue
            } else {
                println("This music item is located locally")
            }
            
            if thisItem.assetURL == nil {
                let itemTitle = thisItem.valueForProperty(MPMediaItemPropertyTitle) as? String
                if let title = itemTitle {
                    warningMesg = "'\(title)' is not available from Music Library. This item may be from Apple Music."
                }
                else {
                    warningMesg = "The item you chose is not available from Music Library. This item may be from Apple Music."
                }
                
                warningInAddingSongTitle = true
                
            } else {
                
                // Save to Core Data
                let appDel: AppDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                
                // Reference ManagedObjectContext
                let context: NSManagedObjectContext = appDel.managedObjectContext!
                let entity = NSEntityDescription.entityForName("Playlist", inManagedObjectContext: context)
                
                // TODO: check for duplicated item
                
                // Create instance of our data model and initialize
                var playItem = Playlist(entity: entity!, insertIntoManagedObjectContext: context)
                
                let persistentID = String(thisItem.persistentID)
                println("Item persistentID: \(persistentID)")
                
                
                playItem.setValue(persistentID, forKey: "item_persistentID")
                
                var error: NSError?
                if !context.save(&error) {
                    println("Could not save \(error), \(error?.userInfo)")
                    abort()
                }
            }
        }
        
        mediaPicker.dismissViewControllerAnimated(true, completion: nil)
        
        if warningInAddingSongTitle {
            g_alert = UIAlertController(title: "Warning", message: warningMesg, preferredStyle: .Alert)
            g_alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: nil))
            self.presentViewController(g_alert, animated: true, completion: nil)
            
            warningInAddingSongTitle = false
        }
        
        loadPlaylistItems()
    }
    
    /**
    media picker has been cancelled
    */
    func mediaPickerDidCancel(mediaPicker: MPMediaPickerController!) {
        mediaPicker.dismissViewControllerAnimated(true, completion: nil)
    }
}
