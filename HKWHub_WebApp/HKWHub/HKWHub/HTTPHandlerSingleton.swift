//
//  HTTPHandlerSingleton.swift
//  HKWHub
//
//  Created by Seonman Kim on 5/26/15.
//  Copyright (c) 2015 Harman International. All rights reserved.
//

import UIKit
import MediaPlayer

class HTTPHandlerSingleton: NSObject, HKWDeviceEventHandlerDelegate, HKWPlayerEventHandlerDelegate {
    var serverURLStr : String!
    var webServer : GCDWebServer!
    // session counter starts from 1000, monotonically increase
    var g_sessionCounter = 1000
    var g_timeElapsed = -1  // -1 : not started
    var g_playbackState = HKPlayerState.EPlayerState_Initialized
    var g_currentPlaybackSession = -1 // -1: no current playback session
    var sessionTimer : NSTimer!
    
    var sessionTable: [Int: SessionInfo] = [Int:SessionInfo]()
    
    class var sharedInstance : HTTPHandlerSingleton {
        struct Static {
            static var onceToken: dispatch_once_t = 0
            static var instance: HTTPHandlerSingleton? = nil
        }
        
        dispatch_once(&Static.onceToken) {
            Static.instance = HTTPHandlerSingleton()
        }
        
        return Static.instance!
    }
    
    
    func initHTTPServer() -> Bool {
        webServer = GCDWebServer()
        
        addHandlers()
        
        var portNo: UInt = 8080
        var retry = 0
        println("Trying to open HTTP server port at: \(portNo)")
        
        var result = webServer.startWithPort(portNo, bonjourName: nil)
        
        while !result && retry < 10 {
            println("failed to open port: \(portNo)")
            sleep(1)

            portNo++
            println("Trying to open HTTP server port again at: \(portNo)")
            result = webServer.startWithPort(portNo, bonjourName: nil)
            retry++
        }
        
        if result {
            println("Server port \(portNo) was successfully opened")
            
            println("webServer.serverURL: \(webServer.serverURL)")

            if webServer.serverURL != nil {
                self.serverURLStr = webServer.serverURL.absoluteString
                println("Visit \(webServer.serverURL) in your web browser")
                
                HKWDeviceEventHandlerSingleton.sharedInstance().delegate = self
                HKWPlayerEventHandlerSingleton.sharedInstance().delegate = self
                
                // start Timer for Session management
                sessionTimer = NSTimer.scheduledTimerWithTimeInterval(60.0, target: self, selector: "checkSessionTimeout:", userInfo: nil, repeats: true)
                
            } else {
                result = false
            }
        }
        
        return result
    }
    
    func topMostController() -> UIViewController {
        var topController = UIApplication.sharedApplication().keyWindow?.rootViewController
        
        while (topController?.presentedViewController != nil) {
        topController = topController?.presentedViewController
        }
        
        return topController!
    }
    
    func addHandlers() {

        // GET methods
        webServer.addDefaultHandlerForMethod("GET", requestClass: GCDWebServerRequest.self, processBlock: { request in
            var urlString = request.URL.absoluteString!
            println("URL: \(urlString)")
            println("path: \(request.path)")
            println("query: \(request.query)")
            var appendedString = "< \(request.remoteAddressString) \(request.path) \(request.query)"
            println("appendedString: \(appendedString)")
            self.printLog(appendedString)
            
            var response : GCDWebServerResponse!
            
            if request.query.count == 0 && request.path != "/v1/init_session" {
                var jsonResponse = NSDictionary(objectsAndKeys:kErrorInvalidParam, kError)
            
                self.printLog("> \(request.remoteAddressString) \(jsonResponse)")
                response = self.createDataResponse(jsonResponse)
            }
            //
            // Session Control
            //
            else if request.path == "/v1/init_session" {
                // init session
                response = self.handleGetInitSession(request)
            } else if request.path == "/v1/close_session" {
                // init session
                response = self.handleGetCloseSession(request)
            }
            
            //
            // Device Management
            //
            else if request.path == "/v1/device_count" {
                response = self.handleGetDeviceCount(request)
                
            } else if request.path == "/v1/device_list" {
                response = self.handleGetDeviceList(request)
                
            } else if request.path == "/v1/device_info" {
                response = self.handleGetDeviceInfo(request)
                
            } else if request.path == "/v1/add_device_to_session" {
                response = self.handleGetAddDeviceToSession(request)
                
            } else if request.path == "/v1/remove_device_from_session" {
                response = self.handleGetRemoveDeviceFromSession(request)
                
            }
            
            //
            // Media Playback Management
            //
            else if request.path == "/v1/media_list" {
                response = self.handleGetMediaList(request)
                
            } else if request.path == "/v1/set_party_mode" {
                response = self.handleGetSetPartyMode(request)
                
            } else if request.path == "/v1/play_hub_media" {
                response = self.handleGetPlayHubMedia(request)
                
            } else if request.path == "/v1/resume_hub_media" {
                response = self.handleGetResumeHubMedia(request)
                
            } else if request.path == "/v1/play_hub_media_party_mode" {
                response = self.handleGetPlayHubMediaPartyMode(request)
                
            } else if request.path == "/v1/resume_hub_media_party_mode" {
                response = self.handleGetResumeHubMediaPartyMode(request)
                
            } else if request.path == "/v1/play_hub_media_selected_speakers" {
                response = self.handleGetPlayHubMediaSelectedSpeakers(request)
                
            } else if request.path == "/v1/resume_hub_media_selected_speakers" {
                response = self.handleGetResumeHubMediaSelectedSpeakers(request)
            }
            else if request.path == "/v1/play_web_media" {
                response = self.handleGetPlayWebMedia(request)
            }
            else if request.path == "/v1/play_web_media_party_mode" {
                response = self.handleGetPlayWebMediaPartyMode(request)
            }
            else if request.path == "/v1/play_web_media_selected_speakers" {
                response = self.handleGetPlayWebMediaSelectedSpeakers(request)
            }
            else if request.path == "/v1/play_local_mp3" {
                response = self.handleGetPlayLocalMP3(request)
                
            } else if request.path == "/v1/play_cached_mp3" {
                response = self.handleGetPlayCachedMP3(request)
            } else if request.path == "/v1/pause_play" {
                response = self.handleGetPausePlay(request)
            }
            else if request.path == "/v1/stop_play" {
                response = self.handleGetStopPlay(request)
                
            } else if request.path == "/v1/playback_status" {
                response = self.handleGetPlaybackStatus(request)
                
            } else if request.path == "/v1/time_elapsed" {
                response = self.handleGetTimeElapsed(request)
                
            } else if request.path == "/v1/is_playing" {
                response = self.handleGetIsPlaying(request)
            }

            //
            // Volume Controls
            //
            else if request.path == "/v1/get_volume" {
                // get volume of all devices
                response = self.handleGetGetVolume(request)
                
            } else if request.path == "/v1/get_volume_device" {
                // get volume of all devices
                response = self.handleGetGetVolumeDevice(request)
                
            } else if request.path == "/v1/set_volume" {
                // set volume of all devices
                response = self.handleGetSetVolume(request)
            } else if request.path == "/v1/set_volume_device" {
                // set volume of all devices
                response = self.handleGetSetVolumeDevice(request)
            }
            else {
                // unknown request
                println("Unknown request path: \(request.path)")
                var jsonResponse = NSDictionary(objectsAndKeys:kErrorUnknownRequest, kError)
                
                self.printLog("> \(request.remoteAddressString) \(jsonResponse)")
                response = self.createDataResponse(jsonResponse)
            }
            
            return response
            
        })
        
    }
    
    func createDataResponse(content: NSDictionary) -> GCDWebServerResponse {
        var response = GCDWebServerDataResponse(JSONObject: content)
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        
        return response
    }
    
    func convertStringToCLongLong(deviceIDStr: String) -> CLongLong {
        let strAsNSString = deviceIDStr as NSString
        let deviceID = strAsNSString.longLongValue
        let uDeviceID = CLongLong(deviceID)
        return uDeviceID
    }
    
    /*
    * Handle: GET /v1/init_session
    */
    func handleGetInitSession(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var priority = kDefaultPriority
        
        if let priorityStr = getParamValue(request, key: kParamPriority) {
            println("priorityStr:[\(priorityStr)]")
            priority = priorityStr.toInt()!
        }
        
        var sessionInfo = newSessionInfo(request, priority: priority)
        
        var jsonResponse = NSDictionary(objectsAndKeys:"\(sessionInfo.sessionID)", kParamSessionID)
        printLog("> \(request.remoteAddressString) \(jsonResponse)")

        // var response = GCDWebServerDataResponse(HTML:"<html><body><p>Hello World</p></body></html>")
        
        return createDataResponse(jsonResponse)
    }
    
    /*
    * Handle: GET /v1/close_session?SessionID=<session id>
    */
    func handleGetCloseSession(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            // remove the session id from the table
            removeSession(request)
            println("remove session")
            jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
    }
    
    /*
    * Handle: GET /v1/device_count?SessionID=<session id>
    */
    func handleGetDeviceCount(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            jsonResponse = NSDictionary(objectsAndKeys:"\(HKWControlHandler.sharedInstance().getDeviceCount())", "DeviceCount")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
    }
    
    /*
     * Handle: GET /v1/device_list?SessionID=<session id>
     */
    func handleGetDeviceList(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            // crate a list of media items
            let listOfDeviceItems = createDeviceList()

            response = GCDWebServerDataResponse(JSONObject: listOfDeviceItems)
            printLog("> \(request.remoteAddressString) \(listOfDeviceItems)")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
            printLog("> \(request.remoteAddressString) \(jsonResponse)")
            response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        }
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    /*
     * Handle : GET /v1/device_info?SessionID=<session id>&DeviceID=<device id>
     */
    func handleGetDeviceInfo(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            var found = false
            if let deviceIDStr = getParamValue(request, key: kParamDeviceID) {
                println("deviceIDStr: \(deviceIDStr)")
                if deviceIDStr != "" {
                    let uDeviceID = convertStringToCLongLong(deviceIDStr)
                    println("uDeviceID: \(uDeviceID)")

                    if let deviceInfo = HKWControlHandler.sharedInstance().getDeviceInfoById(uDeviceID) {
                        var deviceItem = NSMutableDictionary()
                        
                        var deviceIdStr = "\(deviceInfo.deviceId)"
                        var groupIdStr = "\(deviceInfo.groupId)"
                        
                        deviceItem.setObject(deviceIdStr, forKey: "DeviceID")
                        deviceItem.setObject(deviceInfo.deviceName, forKey: "DeviceName")
                        deviceItem.setObject(groupIdStr, forKey: "GroupID")
                        deviceItem.setObject(deviceInfo.groupName, forKey: "GroupName")
                        deviceItem.setObject(deviceInfo.isPlaying, forKey: "IsPlaying")
                        deviceItem.setObject(deviceInfo.ipAddress, forKey: "IPAddress")
                        deviceItem.setObject(deviceInfo.port, forKey: "Port")
                        deviceItem.setObject(deviceInfo.modelName, forKey: "ModelName")
                        deviceItem.setObject(deviceInfo.volume, forKey: "Volume")
                        deviceItem.setObject(deviceInfo.version, forKey: "Version")
                        deviceItem.setObject(deviceInfo.wifiSignalStrength, forKey: "WifiSignalStrength")
                        deviceItem.setObject(deviceInfo.macAddress, forKey: "MacAddress")
                        deviceItem.setObject(deviceInfo.role, forKey: "Role")
                        deviceItem.setObject(deviceInfo.active, forKey: "Active")
                        
                        response = GCDWebServerDataResponse(JSONObject: deviceItem)
                        printLog("> \(request.remoteAddressString) \(deviceItem)")
                        found = true
                    }
                }
            }
            if !found {
                jsonResponse = NSDictionary(objectsAndKeys:kErrorDeviceNotFound, kError)
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                response = GCDWebServerDataResponse(JSONObject: jsonResponse)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
            printLog("> \(request.remoteAddressString) \(jsonResponse)")
            response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        }
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    /*
    * Handle: GET /v1/add_device_to_session?SessionID=<session id>&DeviceID=<device id>
    */
    func handleGetAddDeviceToSession(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            var found = false
            
            if let deviceIDStr = getParamValue(request, key: kParamDeviceID) {
                println("deviceIDStr:[\(deviceIDStr)]")
                let uDeviceID = convertStringToCLongLong(deviceIDStr)
                println("uDeviceID: \(uDeviceID)")

                if HKWControlHandler.sharedInstance().isDeviceAvailable(uDeviceID) {
                    var result = HKWControlHandler.sharedInstance().addDeviceToSession(uDeviceID)
                    jsonResponse = NSDictionary(objectsAndKeys:"\(result)", "Result")
                    found = true
                }
            }
            
            if !found {
                jsonResponse = NSDictionary(objectsAndKeys:kErrorDeviceNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)

    }
    
    /*
     * Handle: GET /v1/remove_device_from_session?SessionID=<session id>&DeviceID=<device id>
     */
    func handleGetRemoveDeviceFromSession(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            var found = false
            
            if let deviceIDStr = getParamValue(request, key: kParamDeviceID) {
                println("deviceIDStr:[\(deviceIDStr)]")
                let uDeviceID = convertStringToCLongLong(deviceIDStr)
                println("uDeviceID: \(uDeviceID)")

                if HKWControlHandler.sharedInstance().isDeviceAvailable(uDeviceID) {
                    var result = HKWControlHandler.sharedInstance().removeDeviceFromSession(uDeviceID)
                    jsonResponse = NSDictionary(objectsAndKeys:"\(result)", "Result")
                    found = true
                }
            }
            if !found {
                jsonResponse = NSDictionary(objectsAndKeys:kErrorDeviceNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)

    }
    
    /*
    * Handle: GET /v1/media_list?SessionID=<session id>
    */
    func handleGetMediaList(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        
        if isSessionValid(request) {
            // crate a list of media items
            let listOfMediaItems = createListOfMediaItems()
            response = GCDWebServerDataResponse(JSONObject: listOfMediaItems)
            printLog("> \(request.remoteAddressString) \(listOfMediaItems)")

        } else {
            var jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
            printLog("> \(request.remoteAddressString) \(jsonResponse)")
            response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        }
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    /*
    * Handle: GET /v1/set_party_mode?SessionID=<session id>
    */
    func handleGetSetPartyMode(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var jsonResponse: NSDictionary!

        if isSessionValid(request) {
            setSpeakersPartyMode()
            
            // check the total speaker available
            if HKWControlHandler.sharedInstance().getActiveDeviceCount() == 0 {
                // no device available to play
                jsonResponse = createDictionayForFailure("No available speaker")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
    }
    
    
    /*
    * Hangle: GET /v1/play_hub_media?SessionID=<session id>&PersistentID=<persistent id>
    */
    func handleGetPlayHubMedia(request: GCDWebServerRequest) -> GCDWebServerResponse {
        return handleGetPlayHubMediaWithResume(request, resumeFlag: false)
    }
    
    /*
    * Hangle: GET /v1/resume_hub_media?SessionID=<session id>&PersistentID=<persistent id>
    */
    func handleGetResumeHubMedia(request: GCDWebServerRequest) -> GCDWebServerResponse {
        return handleGetPlayHubMediaWithResume(request, resumeFlag: true)
    }
    
    /*
    * Hangle: GET /v1/play_hub_media_party_mode?SessionID=<session id>&PersistentID=<persistent id>
    */
    func handleGetPlayHubMediaPartyMode(request: GCDWebServerRequest) -> GCDWebServerResponse {
        return handleGetPlayHubMediaPartyModeWithResume(request, resumeFlag: false)
    }
    
    /*
    * Hangle: GET /v1/resume_hub_media_party_mode?SessionID=<session id>&PersistentID=<persistent id>
    */
    func handleGetResumeHubMediaPartyMode(request: GCDWebServerRequest) -> GCDWebServerResponse {
        return handleGetPlayHubMediaPartyModeWithResume(request, resumeFlag: true)
    }

    /*
    * Hangle: GET /v1/play_hub_media_selected_speakers?SessionID=<session id>&PersistentID=<persistent id>&DeviceIDList=<xxx,xxx,xxx>
    */
    func handleGetPlayHubMediaSelectedSpeakers(request: GCDWebServerRequest) -> GCDWebServerResponse {
        return handleGetPlayHubMediaSelectedSpeakersWithResume(request, resumeFlag: false)
    }
    
    /*
    * Hangle: GET /v1/resume_hub_media_selected_speakers?SessionID=<session id>&PersistentID=<persistent id>&DeviceIDList=<xxx,xxx,xxx>
    */
    func handleGetResumeHubMediaSelectedSpeakers(request: GCDWebServerRequest) -> GCDWebServerResponse {
        return handleGetPlayHubMediaSelectedSpeakersWithResume(request, resumeFlag: true)
    }
    
    
    func handleGetPlayHubMediaWithResume(request: GCDWebServerRequest, resumeFlag: Bool) -> GCDWebServerResponse {

        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        
        if isSessionValid(request) {
            if !comparePriorityOfNewSession(request) {
                jsonResponse = createDictionayForFailure(kFailureReasonLowerPriority)
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            if g_currentPlaybackSession > 0 {
                // there is on-going session
                HKWControlHandler.sharedInstance().stop()
                sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStateStopped
                g_currentPlaybackSession = -1
            }
            
            // check the total speaker available
            if HKWControlHandler.sharedInstance().getActiveDeviceCount() == 0 {
                // no device available to play
                jsonResponse = createDictionayForFailure("No available speaker")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            if let persistentID = getParamValue(request, key: kParamPersistentID) {
                println("persistentID: \(persistentID)")
                
                // query the media library
                let query = MPMediaQuery.songsQuery()
                
                let predicate = MPMediaPropertyPredicate(value: persistentID,
                    forProperty: MPMediaItemPropertyPersistentID)
                query.addFilterPredicate(predicate)
                let item = query.items.first as? MPMediaItem
                
                let assetUrl = item!.assetURL
                let item_title = item!.title
                let item_duration = item!.playbackDuration
                
                println("URLString: \(assetUrl.absoluteString)")
                

                
                if HKWControlHandler.sharedInstance().playCAF(assetUrl, songName: item_title, resumeFlag: resumeFlag) {
                    println("playing")
                    jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
                    
                    g_currentPlaybackSession = getSessionID(request)
                    sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStatePlaying
                    
                } else {
                    println("error in playing")
                    jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                }
            } else {
                jsonResponse = NSDictionary(objectsAndKeys: kErrorPersistentIDNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
    }
    
    func handleGetPlayHubMediaPartyModeWithResume(request: GCDWebServerRequest, resumeFlag: Bool) -> GCDWebServerResponse {
        
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            
            if !comparePriorityOfNewSession(request) {
                jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            
            if g_currentPlaybackSession > 0 {
                // there is on-going session
                HKWControlHandler.sharedInstance().stop()
                sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStateStopped
                g_currentPlaybackSession = -1
            }
            
            // set the speaker as party mode
            setSpeakersPartyMode()
            
            // check the total speaker available
            if HKWControlHandler.sharedInstance().getActiveDeviceCount() == 0 {
                // no device available to play
                jsonResponse = createDictionayForFailure("No available speaker")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            
            if let persistentID = getParamValue(request, key: kParamPersistentID) {
                println("persistentID: \(persistentID)")
                
                // query the media library
                let query = MPMediaQuery.songsQuery()
                
                let predicate = MPMediaPropertyPredicate(value: persistentID,
                    forProperty: MPMediaItemPropertyPersistentID)
                query.addFilterPredicate(predicate)
                let item = query.items.first as? MPMediaItem
                
                let assetUrl = item!.assetURL
                let item_title = item!.title
                let item_duration = item!.playbackDuration
                
                println("URLString: \(assetUrl.absoluteString)")
                
                if HKWControlHandler.sharedInstance().playCAF(assetUrl, songName: item_title, resumeFlag: resumeFlag) {
                    println("playing")
                    jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
                    
                    g_currentPlaybackSession = getSessionID(request)
                    sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStatePlaying
                    
                } else {
                    println("error in playing")
                    jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                }
            } else {
                jsonResponse = NSDictionary(objectsAndKeys: kErrorPersistentIDNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
        
    }

    
    func handleGetPlayHubMediaSelectedSpeakersWithResume(request: GCDWebServerRequest, resumeFlag: Bool) -> GCDWebServerResponse {
        
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            
            if !comparePriorityOfNewSession(request) {
                jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            
            if g_currentPlaybackSession > 0 {
                // there is on-going session
                HKWControlHandler.sharedInstance().stop()
                sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStateStopped
                g_currentPlaybackSession = -1
            }
            
            if let speakerListStr = getParamValue(request, key: kParamDeviceIDList) {
                let speakerList = speakerListStr.componentsSeparatedByString(",")
                
                if speakerList.count == 0 {
                    // return error: no speaker in param
                    jsonResponse = createDictionayForFailure("No speaker in SpeakerList parameter")
                    printLog("> \(request.remoteAddressString) \(jsonResponse)")
                    return createDataResponse(jsonResponse)
                }
                
                addSpeakerListToSession(speakerList)
            } else {
                jsonResponse = NSDictionary(objectsAndKeys: kErrorDeviceIDListlNotFound, kError)
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }

            // check the total speaker available
            if HKWControlHandler.sharedInstance().getActiveDeviceCount() == 0 {
                // no device available to play
                jsonResponse = createDictionayForFailure("No available speaker")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            
            if let persistentID = getParamValue(request, key: kParamPersistentID) {
                println("persistentID: \(persistentID)")
                
                // query the media library
                let query = MPMediaQuery.songsQuery()
                
                let predicate = MPMediaPropertyPredicate(value: persistentID,
                    forProperty: MPMediaItemPropertyPersistentID)
                query.addFilterPredicate(predicate)
                let item = query.items.first as? MPMediaItem
                
                let assetUrl = item!.assetURL
                let item_title = item!.title
                let item_duration = item!.playbackDuration
                
                println("URLString: \(assetUrl.absoluteString)")
                
                if HKWControlHandler.sharedInstance().playCAF(assetUrl, songName: item_title, resumeFlag: resumeFlag) {
                    println("playing")
                    jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
                    
                    g_currentPlaybackSession = getSessionID(request)
                    sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStatePlaying
                    
                } else {
                    println("error in playing")
                    jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                }
            } else {
                jsonResponse = NSDictionary(objectsAndKeys: kErrorPersistentIDNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
        
    }
   
    /*
    * Hangle: GET /v1/play_web_media?SessionID=<session id>&URL=<url>
    */
    func handleGetPlayWebMedia(request: GCDWebServerRequest) -> GCDWebServerResponse {
        
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            if !comparePriorityOfNewSession(request) {
                jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            if g_currentPlaybackSession > 0 {
                // there is on-going session
                HKWControlHandler.sharedInstance().stop()
                sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStateStopped
                g_currentPlaybackSession = -1
            }
            
            // check the total speaker available
            if HKWControlHandler.sharedInstance().getActiveDeviceCount() == 0 {
                // no device available to play
                jsonResponse = createDictionayForFailure("No available speaker")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            if let mediaUrl = getParamValue(request, key: kParamMediaUrl) {
                println("mediaUrl: \(mediaUrl)")
                
                var g_result = false
                var done = false
                HKWControlHandler.sharedInstance().playStreamingMedia(mediaUrl, withCallback: {(bool result) -> Void in
                    
                    g_result = result
                    done = true
                })
                
                while (!done) {
                    usleep(1000)
                }
                
                if g_result {
                    println("playStreamingMedia: successful")
                    jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
                    g_currentPlaybackSession = getSessionID(request)
                    sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStatePlaying

                } else {
                    println("playStreamingMedia: failed")
                    jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                }

            } else {
                jsonResponse = NSDictionary(objectsAndKeys: kErrorMediaUrlNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
        
    }
    
    /*
    * Hangle: GET /v1/play_web_media_party_mode?SessionID=<session id>&URL=<url>
    */
    func handleGetPlayWebMediaPartyMode(request: GCDWebServerRequest) -> GCDWebServerResponse {
        
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            if !comparePriorityOfNewSession(request) {
                jsonResponse = createDictionayForFailure(kFailureReasonLowerPriority)
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            if g_currentPlaybackSession > 0 {
                // there is on-going session
                HKWControlHandler.sharedInstance().stop()
                sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStateStopped
                g_currentPlaybackSession = -1
            }
            
            // set the speaker as party mode
            setSpeakersPartyMode()
            
            // check the total speaker available
            if HKWControlHandler.sharedInstance().getActiveDeviceCount() == 0 {
                // no device available to play
                jsonResponse = createDictionayForFailure("No available speaker")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
        
            if let mediaUrl = getParamValue(request, key: kParamMediaUrl) {
                println("mediaUrl: \(mediaUrl)")
                
                var g_result = false
                var done = false
                HKWControlHandler.sharedInstance().playStreamingMedia(mediaUrl, withCallback: {(bool result) -> Void in
                    
                    g_result = result
                    done = true
                })
                
                while (!done) {
                    usleep(1000)
                }
                
                if g_result {
                    println("playStreamingMedia: successful")
                    jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
                    g_currentPlaybackSession = getSessionID(request)
                    sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStatePlaying
                    
                } else {
                    println("playStreamingMedia: failed")
                    jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                }
                
            } else {
                jsonResponse = NSDictionary(objectsAndKeys: kErrorMediaUrlNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
        
    }
    
    
    /*
    * Hangle: GET /v1/play_web_media_selected_speakers?SessionID=<session id>&URL=<url>&DeviceIDList=<xxx,xxx>
    */
    func handleGetPlayWebMediaSelectedSpeakers(request: GCDWebServerRequest) -> GCDWebServerResponse {
        
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            if !comparePriorityOfNewSession(request) {
                jsonResponse = createDictionayForFailure(kFailureReasonLowerPriority)
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            if g_currentPlaybackSession > 0 {
                // there is on-going session
                HKWControlHandler.sharedInstance().stop()
                sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStateStopped
                g_currentPlaybackSession = -1
            }
            
            if let speakerListStr = getParamValue(request, key: kParamDeviceIDList) {
                let speakerList = speakerListStr.componentsSeparatedByString(",")
                
                if speakerList.count == 0 {
                    // return error: no speaker in param
                    jsonResponse = createDictionayForFailure("No speaker in SpeakerList parameter")
                    printLog("> \(request.remoteAddressString) \(jsonResponse)")
                    return createDataResponse(jsonResponse)
                }
                
                addSpeakerListToSession(speakerList)
            } else {
                jsonResponse = NSDictionary(objectsAndKeys: kErrorDeviceIDListlNotFound, kError)
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            
            // check the total speaker available
            if HKWControlHandler.sharedInstance().getActiveDeviceCount() == 0 {
                // no device available to play
                jsonResponse = createDictionayForFailure("No available speaker")
                printLog("> \(request.remoteAddressString) \(jsonResponse)")
                return createDataResponse(jsonResponse)
            }
            

            if let mediaUrl = getParamValue(request, key: kParamMediaUrl) {
                println("mediaUrl: \(mediaUrl)")
                
                var g_result = false
                var done = false
                HKWControlHandler.sharedInstance().playStreamingMedia(mediaUrl, withCallback: {(bool result) -> Void in
                    
                    g_result = result
                    done = true
                })
                
                while (!done) {
                    usleep(1000)
                }
                
                if g_result {
                    println("playStreamingMedia: successful")
                    jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
                    g_currentPlaybackSession = getSessionID(request)
                    sessionTable[g_currentPlaybackSession]?.playbackStatus = kPlayerStatePlaying
                    
                } else {
                    println("playStreamingMedia: failed")
                    jsonResponse = NSDictionary(objectsAndKeys:"false", "Result")
                }
                
            } else {
                jsonResponse = NSDictionary(objectsAndKeys: kErrorMediaUrlNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
        
    }
    
    
    func handleGetPlayLocalMP3(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            jsonResponse = NSDictionary(objectsAndKeys:"Not Implemented", "Error")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:"SessionNotFound", "Error")
        }
        
        logString = "> \(request.remoteAddressString) \(jsonResponse)"
        response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        printLog(logString)
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    
    func handleGetPlayCachedMP3(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            jsonResponse = NSDictionary(objectsAndKeys:"Not Implemented", "Error")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:"SessionNotFound", "Error")
        }
        
        logString = "> \(request.remoteAddressString) \(jsonResponse)"
        response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        printLog(logString)
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    /*
     * Hangle: GET /v1/pause_play?SessionID=<session id>
     */
    func handleGetPausePlay(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            HKWControlHandler.sharedInstance().pause()
            jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
            g_currentPlaybackSession = -1
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
    }
    
    /*
    * Hangle: GET /v1/stop_play?SessionID=<session id>
    */
    func handleGetStopPlay(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            HKWControlHandler.sharedInstance().stop()
            jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
            g_currentPlaybackSession = -1
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
    }

    func convertPlayerState(playerState: HKPlayerState) -> String {
        var playStatus: String!

        switch playerState {
        case .EPlayerState_Initialized: playStatus = kPlayerStateInitialized
        case .EPlayerState_Playing: playStatus = kPlayerStatePlaying
        case .EPlayerState_Paused: playStatus = kPlayerStatePaused
        case .EPlayerState_Stopped: playStatus = kPlayerStateStopped
        default: playStatus = kPlayerStateNotDefined
        }
        
        return playStatus
    }
    /*
        1. PlayerStateInitialized: -1
        2. PlayerStatePlaying : -1  (play started, but audio is not playing yet.)
        3. PlayerStatePlaying : 1, ...  (now playing audio)
        // pause
        4. PlayerStatePaused : 60
        // Resume
        5. PlayerStatePlaying: 62, ...
        // When it stop
        6. PlayerStateStopped:
    */
    func handleGetPlaybackStatus(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            let sessionID = getSessionID(request)
            if let playStatus = sessionTable[sessionID]?.playbackStatus {
                jsonResponse = NSDictionary(objects: ["\(playStatus)", "\(g_timeElapsed)"], forKeys: ["PlaybackState", "TimeElapsed"])
            } else {
                jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)
    }
    
    func handleGetTimeElapsed(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            jsonResponse = NSDictionary(objectsAndKeys:"\(g_timeElapsed)", "TimeElapsed")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        logString = "> \(request.remoteAddressString) \(jsonResponse)"
        response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        printLog(logString)
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    func handleGetIsPlaying(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            jsonResponse = NSDictionary(objectsAndKeys:"\(HKWControlHandler.sharedInstance().isPlaying())", "IsPlaying")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        printLog("> \(request.remoteAddressString) \(jsonResponse)")
        return createDataResponse(jsonResponse)

    }
    
    //
    // Volume Control APIs
    //
    func handleGetGetVolume(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            jsonResponse = NSDictionary(objectsAndKeys:"\(HKWControlHandler.sharedInstance().getVolume())", "Volume")
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        logString = "> \(request.remoteAddressString) \(jsonResponse)"
        response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        printLog(logString)
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    func handleGetGetVolumeDevice(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            var found = false
            if let deviceIDStr = getParamValue(request, key: kParamDeviceID) {
                let uDeviceID = convertStringToCLongLong(deviceIDStr)
                println("uDeviceID: \(uDeviceID)")

                if HKWControlHandler.sharedInstance().isDeviceAvailable(uDeviceID) {
                    var volume = HKWControlHandler.sharedInstance().getDeviceVolume(uDeviceID)
                    jsonResponse = NSDictionary(objectsAndKeys:"\(volume)", "Volume")
                    found = true
                }
            }
            
            if !found {
                jsonResponse = NSDictionary(objectsAndKeys:kErrorDeviceNotFound, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        logString = "> \(request.remoteAddressString) \(jsonResponse)"
        response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        printLog(logString)
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    func handleGetSetVolume(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            var found = false
            var result = false
            if let volumeStr = getParamValue(request, key: kParamVolume) {
                if let volume = volumeStr.toInt() {
                    println("setVolume: \(volume)")
                    HKWControlHandler.sharedInstance().setVolume(volume)
                    result = true
                    jsonResponse = NSDictionary(objectsAndKeys:"\(result)", "Result")

                    found = true
                }
            }
            if !found {
                jsonResponse = NSDictionary(objectsAndKeys:kErrorInvalidParam, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        logString = "> \(request.remoteAddressString) \(jsonResponse)"
        response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        printLog(logString)
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    func handleGetSetVolumeDevice(request: GCDWebServerRequest) -> GCDWebServerResponse {
        var response: GCDWebServerDataResponse!
        var logString: String!
        var jsonResponse: NSDictionary!
        
        if isSessionValid(request) {
            var found = false
            if let deviceIDStr = getParamValue(request, key: kParamDeviceID) {
                let uDeviceID = convertStringToCLongLong(deviceIDStr)
                println("uDeviceID: \(uDeviceID)")

                if HKWControlHandler.sharedInstance().isDeviceAvailable(uDeviceID) {
                    if let volumeStr = getParamValue(request, key: kParamVolume) {
                        if let volume = volumeStr.toInt() {
                            HKWControlHandler.sharedInstance().setVolumeDevice(uDeviceID, volume: volume)
                            jsonResponse = NSDictionary(objectsAndKeys:"true", "Result")
                            found = true
                        }
                    }
                }
            }
            
            if !found {
                jsonResponse = NSDictionary(objectsAndKeys:kErrorInvalidParam, kError)
            }
            
        } else {
            jsonResponse = NSDictionary(objectsAndKeys:kErrorSessionNotFound, kError)
        }
        
        logString = "> \(request.remoteAddressString) \(jsonResponse)"
        response = GCDWebServerDataResponse(JSONObject: jsonResponse)
        printLog(logString)
        
        response.setValue("*", forAdditionalHeader: "Access-Control-Allow-Origin")
        return response
    }
    
    
    func createListOfMediaItems() -> NSMutableDictionary {
        var listOfItems =  NSMutableArray()
        
        for playList in g_playList {
            var mediaItem = NSMutableDictionary()
            mediaItem.setObject(playList.item_persistentID, forKey: "PersistentID")
            mediaItem.setObject(playList.item_title, forKey: "Title")
            mediaItem.setObject(playList.item_artist, forKey: "Artist")
            mediaItem.setObject(playList.item_album_title, forKey: "AlbumTitle")
            mediaItem.setObject(Int(playList.item_duration), forKey: "Duration")

            // we need to revisit this - converting UIImage to JSON PNG data
//            mediaItem["Artwork"] = UIImagePNGRepresentation(playList.artworkImageBig)
//            listOfItems.append(mediaItem)
            listOfItems.addObject(mediaItem)
        }
        
        var finalList = NSMutableDictionary()
        finalList.setObject(listOfItems, forKey: "MediaList")
        println("finalList: \(finalList)")
        return finalList
    }
    
    func createDeviceList() -> NSMutableDictionary {
        var deviceList = NSMutableArray()
        
        let deviceCount = HKWControlHandler.sharedInstance().getDeviceCount()
        for var i = 0; i < deviceCount; i++ {
            let deviceInfo = HKWControlHandler.sharedInstance().getDeviceInfoByIndex(i)
            var deviceItem = NSMutableDictionary()
            println("deviceID: \(deviceInfo.deviceId)")
            var deviceIdStr = "\(deviceInfo.deviceId)"
            var groupIdStr = "\(deviceInfo.groupId)"
            deviceItem.setObject(deviceIdStr, forKey: "DeviceID")
            deviceItem.setObject(deviceInfo.deviceName, forKey: "DeviceName")
            deviceItem.setObject(groupIdStr, forKey: "GroupID")
            deviceItem.setObject(deviceInfo.groupName, forKey: "GroupName")
            deviceItem.setObject(deviceInfo.isPlaying, forKey: "IsPlaying")
            deviceItem.setObject(deviceInfo.ipAddress, forKey: "IPAddress")
            deviceItem.setObject(deviceInfo.port, forKey: "Port")
            deviceItem.setObject(deviceInfo.modelName, forKey: "ModelName")
            deviceItem.setObject(deviceInfo.volume, forKey: "Volume")
            deviceItem.setObject(deviceInfo.version, forKey: "Version")
            deviceItem.setObject(deviceInfo.wifiSignalStrength, forKey: "WifiSignalStrength")
            deviceItem.setObject(deviceInfo.macAddress, forKey: "MacAddress")
            deviceItem.setObject(deviceInfo.role, forKey: "Role")
            deviceItem.setObject(deviceInfo.active, forKey: "Active")
            
            deviceList.addObject(deviceItem)
        }
        
        var finalList = NSMutableDictionary()
        finalList.setObject(deviceList, forKey: "DeviceList")
        println("finalList: \(finalList)")
        return finalList
    }
    
    func getParamValue2(request: GCDWebServerRequest, key: String) -> NSString {
        let query = request.query
        if let value = query[key] as? NSString {
            return value
        } else {
            return ""
        }
    }
    
    func getParamValue(request: GCDWebServerRequest, key: String) -> String? {
        let query = request.query
        if let value = query[key] as? String {
            return value
        } else {
            return nil
        }
    }
    
    func getParamValue3(request: GCDWebServerRequest, key: String) -> AnyObject? {
        let query = request.query
        return query[key]
    }
    
    
    func newSessionInfo(request: GCDWebServerRequest, priority: Int) -> SessionInfo {
        var newSession = SessionInfo()
        newSession.remoteAddressString = request.remoteAddressString
        newSession.timeStamp = NSDate()
        newSession.sessionID = g_sessionCounter
        newSession.priority = priority
        
        sessionTable[g_sessionCounter] = newSession
        
        g_sessionCounter++
        
        return newSession
    }
    
    func isSessionValid(request: GCDWebServerRequest) -> Bool {
        let sessionID = getSessionID(request)
        if sessionID >= 0 {
            if let sessionInfo = sessionTable[sessionID] {
                // update timeStamp
                sessionInfo.timeStamp = NSDate()
                return true
            }
        }
        
        return false
    }
    
    func currentPlaybackSession() -> Int {
        if HKWControlHandler.sharedInstance().isPlaying() {
            return g_currentPlaybackSession
        } else {
            return -1
        }
    }
    
    func removeSession(request: GCDWebServerRequest) {
        let sessionID = getSessionID(request)
        if sessionID >= 0 {
            sessionTable.removeValueForKey(sessionID)
        }
    }
    
    func getSessionID(request: GCDWebServerRequest) -> Int {
        let query = request.query
        if let sessionIDStr = query["SessionID"] as? String {
            if let sessionID = sessionIDStr.toInt() {
                return sessionID
            }
        }
        
        return -1
    }
    
    // If the timestamp of the session is expired by 60 min, then the session is removed from the table.
    func checkSessionTimeout(paramTimer: NSTimer) {
        println("check timeout of sessions")
        for session in sessionTable {
            let sessionInfo = session.1
            let timeStamp = sessionInfo.timeStamp
            
            let timeInterval = NSDate().timeIntervalSinceDate(timeStamp)
            println("timeInterval: \(timeInterval)")
            if timeInterval > kSessionTimeoutInterval {
                // The session has expired, so we remove it from the table.
                println("session \(session.0) has been removed.")
                sessionTable.removeValueForKey(session.0)
            }
        }
    }
    
    // compare the priorities of current and next session, and
    // return true if new priorit is higher than the current one
    func comparePriorityOfNewSession(request: GCDWebServerRequest) -> Bool {
        if g_currentPlaybackSession != -1 {
            // the current session is playing the music, so let's compare the priorities
            if let priorityOfCurrentSession = sessionTable[g_currentPlaybackSession]?.priority {
                let sessionID = getSessionID(request)
                if let priorityOfNewSuession = sessionTable[sessionID]?.priority {
                    
                    println("currentSession: \(priorityOfCurrentSession)")
                    println("newSession: \(priorityOfNewSuession)")
                    if priorityOfCurrentSession > priorityOfNewSuession {
                        return false
                    }
                }
            }
        }
        
        return true
    }
    
    func setSpeakersPartyMode() {
        var count = HKWControlHandler.sharedInstance().getDeviceCount()
        for var i = 0; i < count; i++ {
            let deviceInfo = HKWControlHandler.sharedInstance().getDeviceInfoByIndex(i)
            HKWControlHandler.sharedInstance().addDeviceToSession(deviceInfo.deviceId)
            println("Speaker added: \(deviceInfo.deviceName)")
        }
    }
    
    func createDictionayForFailure(reason: String) -> NSDictionary! {
        let jsonResponse = NSDictionary(objects: ["false", reason], forKeys: ["Result", "Reason"])
        return jsonResponse
    }
    
    func printLog(str: String) {
        println("Log: \(str)")
        dispatch_async(dispatch_get_main_queue(), {
            NSNotificationCenter.defaultCenter().postNotificationName(kAppendLogNotification, object: str)
        })
    }
    
    
    func addSpeakerListToSession(speakerList: [String]) {
        let deviceCount = HKWControlHandler.sharedInstance().getDeviceCount()
        for var i = 0; i < deviceCount; i++ {
            let deviceInfo = HKWControlHandler.sharedInstance().getDeviceInfoByIndex(i)
            
            var found = false
            for var j = 0; j < speakerList.count; j++ {
                let deviceId = convertStringToCLongLong(speakerList[j])
                if deviceInfo.deviceId == deviceId {
                    found = true
                    break
                }
            }
            
            if found {
                HKWControlHandler.sharedInstance().addDeviceToSession(deviceInfo.deviceId)
            } else {
                HKWControlHandler.sharedInstance().removeDeviceFromSession(deviceInfo.deviceId)
            }
        }
        
        
        for var i = 0; i < speakerList.count; i++ {
            let deviceId = convertStringToCLongLong(speakerList[i])
            println("deviceID: \(deviceId)")
            HKWControlHandler.sharedInstance().addDeviceToSession(deviceId)
        }
    }
    
    
    func hkwDeviceStateUpdated(deviceId: Int64, withReason reason: Int) {
    }
    
    func hkwErrorOccurred(errorCode: Int, withErrorMessage errorMesg: String!) {
//        g_playbackInterrupted = true
    }
    
    func hkwPlaybackTimeChanged(timeElapsed: Int) {
        g_timeElapsed = timeElapsed
    }
    
    func hkwPlaybackStateChanged(playState: HKPlayerState) {
//        g_playbackState = HKPlayerState(rawValue: playState)!
        g_playbackState = playState
        if g_currentPlaybackSession > 0 {
            sessionTable[g_currentPlaybackSession]?.playbackStatus = convertPlayerState(g_playbackState)
            println("playbackStatus: \(sessionTable[g_currentPlaybackSession]?.playbackStatus)")
        }

        println("+++++++++++++++++++ PlaybackStateChanged: \(HKWControlHandler.sharedInstance().getPlayStateString(playState))")
        
    }
    
    func hkwPlayEnded() {
        println("hkwPlayEnded() in HTTPHandlerSingleton")
        g_timeElapsed = -1
    }
}
