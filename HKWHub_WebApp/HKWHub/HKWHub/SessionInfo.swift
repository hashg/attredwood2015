//
//  SessionInfo.swift
//  HKWHub
//
//  Created by Seonman Kim on 5/26/15.
//  Copyright (c) 2015 Harman International. All rights reserved.
//

import Foundation

class SessionInfo {
    var remoteAddressString : String!
    var timeStamp: NSDate!
    var sessionID: Int = 0
    var priority: Int = 100
    var playbackStatus = kPlayerStateNotDefined
}