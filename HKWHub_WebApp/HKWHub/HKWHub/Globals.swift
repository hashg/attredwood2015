//
//  Globals.swift
//  HKWHub
//
//  Created by Seonman Kim on 6/2/15.
//  Copyright (c) 2015 Harman International. All rights reserved.
//

import Foundation

let kParamSessionID = "SessionID"
let kParamDeviceID = "DeviceID"
let kParamPersistentID = "PersistentID"
let kParamVolume = "Volume"
let kParamMediaUrl = "MediaUrl"
let kParamPriority = "Priority"
let kParamDeviceIDList = "DeviceIDList"

let kError = "Error"
let kErrorDeviceNotFound = "DeviceNotFound"
let kErrorSessionNotFound = "SessionNotFound"
let kErrorPersistentIDNotFound = "PersistentIDNotFound"
let kErrorMediaUrlNotFound = "MediaUrlNotFound"
let kErrorDeviceIDListlNotFound = "DeviceIDListNotFound"
let kErrorInvalidParam = "InvalidParam"
let kErrorUnknownRequest = "UnknownRequest"

let kPlayerStateInitialized = "PlayerStateInitialized"
let kPlayerStatePlaying = "PlayerStatePlaying"
let kPlayerStatePaused = "PlayerStatePaused"
let kPlayerStateStopped = "PlayerStateStopped"
let kPlayerStateNotDefined = "PlayerStateNotDefined"

// failure reason
let kFailureReasonLowerPriority = "The priority of the session is lower than current one"

let kSessionTimeoutInterval = 3600.0

let kDefaultPriority = 100
