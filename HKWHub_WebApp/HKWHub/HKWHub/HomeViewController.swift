//
//  HomeViewController.swift
//  HKWHub
//
//  Created by Seonman Kim on 5/26/15.
//  Copyright (c) 2015 Harman International. All rights reserved.
//

import UIKit

var g_homeVC : HomeViewController!

let kAppendLogNotification = "AppendLogNotification"
var g_logs: String!

class HomeViewController: UIViewController {
    
    @IBOutlet var urlLabel: UILabel!
    @IBOutlet var logsTextView: UITextView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        
        if !g_serverOpened {
            println("Failed to open Server port.")
            g_alert = UIAlertController(title: "Error", message: "Could not open server port. Please check if your phone is connected to Wi-Fi network.", preferredStyle: .Alert)
            g_alert.addAction(UIAlertAction(title: "OK", style: UIAlertActionStyle.Default, handler: { (uiAlertAction: UIAlertAction!) -> Void in
                exit(0)
            }))
            
            self.presentViewController(g_alert, animated: true, completion: nil)
        }
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "handleAppendLogNotification:", name: kAppendLogNotification, object: nil)
        
        urlLabel.text = HTTPHandlerSingleton.sharedInstance.serverURLStr
        logsTextView.text = g_logs
        g_homeVC = self
        
        if !HKWControlHandler.sharedInstance().isInitialized() {
            // show the network initialization dialog
            g_alert = UIAlertController(title: "Initializing", message: "If this dialog does not disappear, please check if any other HK WirelessHD App is running on the phone and kill it. Or, your phone is not in a Wifi network.", preferredStyle: .Alert)
            
            self.presentViewController(g_alert, animated: true, completion: nil)
        }
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        println("viewDidAppear")
        
        if !HKWControlHandler.sharedInstance().initializing() && !HKWControlHandler.sharedInstance().isInitialized() {
            println("initializing in PlaylistTVC")
            
            dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0), {
                if HKWControlHandler.sharedInstance().initializeHKWirelessController(g_licenseKey, withSpeakersAdded: false) != 0 {
                    println("initializeHKWirelessControl failed : invalid license key")
                    return
                }
                println("initializeHKWirelessControl - OK");
                
                // dismiss the network initialization dialog
                if g_alert != nil {
                    g_alert.dismissViewControllerAnimated(true, completion: nil)
                }
                
            })
        }
    }

    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    @IBAction func showMenu(sender: AnyObject) {
        // Dismiss keyboard (optional)
        self.view.endEditing(true )
        self.frostedViewController.view.endEditing(true )
        
        // Present the view controller
        self.frostedViewController.presentMenuViewController()
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

    func handleAppendLogNotification(notification: NSNotification) {
        let date = NSDate()
        let formatter = NSDateFormatter()
        formatter.timeStyle = .MediumStyle
        var time = formatter.stringFromDate(date)
        var split = time.componentsSeparatedByString(" ")
        
        let log = notification.object as! String
        var appendedString = "[\(split[0])] \(log)\n"
        
        var textLength = logsTextView.text.lengthOfBytesUsingEncoding(NSUTF16StringEncoding);
        if textLength > 3000 {
            logsTextView.text = ""
        }
        
        g_logs = logsTextView.text.stringByAppendingString(appendedString)
        logsTextView.text = g_logs

        // scroll the text view to at the end
        var range: NSRange = NSMakeRange(logsTextView.text.lengthOfBytesUsingEncoding(NSUTF16StringEncoding) - 1, 1)
        logsTextView.scrollRangeToVisible(range)

    }
}
