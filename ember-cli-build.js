/* global require, module */
var EmberApp = require('ember-cli/lib/broccoli/ember-app');
var mergeTrees = require('broccoli-merge-trees');
var pickFiles = require('broccoli-static-compiler');
var EmberApp = require('ember-cli/lib/broccoli/ember-app');

module.exports = function(defaults) {
  var app = new EmberApp(defaults, {
    // Add options here
  });

  // app.import('bower_components/ratchet/js/modals.js');
  // app.import('bower_components/ratchet/js/popovers.js');
  // app.import('bower_components/ratchet/js/segmented-controllers.js');
  // app.import('bower_components/ratchet/js/sliders.js');
  // app.import('bower_components/ratchet/js/toggles.js');

  app.import('bower_components/fastclick/lib/fastclick.js');

  var fonts = pickFiles('bower_components/ratchet/fonts', {
    srcDir: '/',
    files: ['ratchicons.eot', 'ratchicons.svg', 'ratchicons.ttf', 'ratchicons.woff'],
    destDir: '/fonts'
  });

  var fontawesome = pickFiles('bower_components/fontawesome/fonts', {
    srcDir: '/',
    files: ['fontawesome-webfont.eot', 'fontawesome-webfont.svg', 'fontawesome-webfont.ttf', 'fontawesome-webfont.woff', 'fontawesome-webfont.woff2'],
    destDir: '/fonts'
  });

  return mergeTrees([app.toTree(), fonts, fontawesome]);
};
