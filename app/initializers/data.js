export function initialize(registry, application) {
  application.inject('route', 'data', 'service:data');
  application.inject('controller', 'data', 'service:data');
  application.inject('component', 'data', 'service:data');
}

export default {
  name: 'data',
  initialize: initialize
};
