export function initialize(registry, application) {
  application.inject('route', 'beacon', 'service:beacon');
  application.inject('controller', 'beacon', 'service:beacon');
  application.inject('component', 'beacon', 'service:beacon');
}

export default {
  name: 'beacon',
  initialize: initialize
};
