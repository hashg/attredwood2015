import Ember from 'ember';

export function initialize(/* container, application */) {
  Ember.run.schedule('afterRender', function() {
    FastClick.attach(document.body);
  });
}

export default {
  name: 'fastclick',
  initialize: initialize
};