import Ember from 'ember';

export default Ember.Controller.extend({
  beacon: Ember.inject.service(),
  hwhub: Ember.inject.service(),

  appName : 'FollowME',
  // isBluetoothOff : Ember.computed.not('beacon.isBluetoothOn'),
  // isBeaconAuthorisedOff : Ember.computed.not('beacon.isBeaconAuthorised'),
  
  volume: 20,
  isPlaying : false,
  isPaused : false,

  isSetting : false,


  speaker_now : null,
  speaker_prev : null,
  song_now : null,

  proximity : null,
  _beacon : null,

  beaconSwitch : function() {
    // console.log('Controller:beaconSwitch');
    var self = this;

    var metadata = self.store.findAll('beacon');
    metadata.then( function(data) {
      var beacon = self.get('beacon.mNearestBeacon');
      for (var i = 0; i < data.get('length'); i++) 
      {
        if( beacon && data.objectAt(i).get('minor') === beacon.minor  && beacon.accuracy > 0)
        {
          self.set('proximity', data.objectAt(i));
          // return data.objectAt(i);
        }
      }
    });
  }.observes('beacon.mNearestBeacon'),



  cBeacon : function() {
    var beacon = this.get('beacon.mNearestBeacon');
    this.set('_beacon', beacon);
  }.observes('beacon.mNearestBeacon'),

  switchMusic : function() {
    console.log('switchMusic');
    var self = this;
    var isPlaying = this.get('isPlaying');

    if(isPlaying) 
    {
      var hub = this.get('hwhub');
      var now = this.get('proximity').get('deviceid');
      var prev = this.get('speaker_now');
      var proximity = this.get('proximity');
      console.log('now : ' + now);
      console.log('prev : ' + prev);

      hub.setVolumeDevice(this.get('volume'), now, true).then( function() { 
        hub.setVolumeDevice(0, prev);
        self.set('speaker_now', now);
        self.set('speaker_prev', prev);
      });
    }

  }.observes('proximity'),

  actions: {
    enable: function() {
      this.get('beacon').startMonitoringAndRanging();
    },
    setVolume: function(flag)
    {
      var hub = this.get('hwhub');
      var speaker = this.get('speaker_now');
      var volume;
      if (flag)
      {//increase
        volume = this.get('volume') + 5;
      }
      else
      {//decrease
        volume = this.get('volume') - 5;
      }


      if(speaker !== "NIL") {
        hub.setVolumeDevice(volume, this.get('speaker_now'));
      }
      else
      {
        hub.setVolumeParty(volume);
      }
      this.set('volume', volume);
    },

    playParty: function(song) {
      var self = this;
      var hub = this.get('hwhub');
      
      var proximity = this.get('proximity');
      if( proximity )
      {

        hub.playParty(song, true).then(function() {
          hub.setVolumeParty(0);
          hub.setVolumeDevice(self.get('volume'), proximity.get('deviceid'));
          self.set('speaker_now', proximity.get('deviceid'));
        });

      }
      else
      {
        hub.playParty(song);
        hub.setVolumeParty(this.get('volume'));
        self.set('speaker_now', "NIL");
      }
      this.set('isPlaying', true);
    },

    pause: function() {
      this.get('hwhub').pause();
      this.set('isPlaying', false);
    },

    stop: function() {
      this.get('hwhub').stop();
      this.set('isPlaying', false);
      this.set('speaker_now', null);
      this.set('speaker_prev', null);
    },

    toggleSettings: function() {
      this.toggleProperty('isSetting');
    },

  }
});
