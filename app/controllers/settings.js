import Ember from 'ember';

export default Ember.Controller.extend({
  hwhub: Ember.inject.service(),
  saved: false,
  

  actions : {
    save: function() {
      localStorage.setItem('server', this.get('server'));
      localStorage.setItem('port', this.get('port'));
      localStorage.setItem('version', this.get('version'));

      var hub = this.get('hwhub');      
      hub.setProperties({
        server: this.get('server'),
        port: this.get('port'),
        version: this.get('version')
      });

      this.set('saved', true);
    }
  }
});
