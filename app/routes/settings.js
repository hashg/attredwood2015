import Ember from 'ember';

export default Ember.Route.extend({
  hwhub: Ember.inject.service(),
  afterModel: function(model) {
    var hwhub = this.get('hwhub');

    this.controllerFor('settings').set('server', hwhub.get('server'));
    this.controllerFor('settings').set('port', hwhub.get('port'));
    this.controllerFor('settings').set('version', hwhub.get('version'));
  }
});
