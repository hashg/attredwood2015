import Ember from 'ember';

export default Ember.Route.extend({
  hwhub: Ember.inject.service(),

  
  model: function() {
    console.log('application::model');
    var self = this;
    var hwhubService = this.get('hwhub');

    var server = localStorage.getItem('server');
    if( !server )
    {
      localStorage.setItem('server', "192.168.1.84");
      server = "192.168.1.84";
    }
    hwhubService.set('server', server);

    var port = localStorage.getItem('port');
    if( !port )
    {
      localStorage.setItem('port', "8080");
      port = "8080";
    }
    hwhubService.set('port', port);

    var version = localStorage.getItem('version');
    if( !version )
    {
      localStorage.setItem('version', "v1");
      version = "v1";
    }
    hwhubService.set('version', version);


    var gotSession = function(data)
    {
      // console.log('gotSession');
      self.get('hwhub').set('session', data.SessionID);
      return Ember.RSVP.hash({
        speakers: self.get('hwhub').speakers_list(),
        songs: self.get('hwhub').songs_list()
      }).then(function(data){
        // console.log(data);
        if(data)
        {
          if(data.speakers && data.speakers.DeviceList && data.speakers.DeviceList.length > 0 )
          {
            data.speakers.DeviceList.forEach(function(item, index) {
              // console.log(item);
              self.store.findAll('beacon').then( function(bcn) {
                for (var i = 0; i < 2; i++) {
                  if( i === index )
                  {
                    bcn.objectAt(i).set('deviceid', item.DeviceID);
                    bcn.objectAt(i).set('devicename', item.GroupName);
                  }
                }

              });
              self.store.push({
                data: {
                  id: item.DeviceID, 
                  type: 'speakers', 
                  attributes: item
                }
              });
            });
          }
          if(data.songs && data.songs.MediaList && data.songs.MediaList.length > 0)
          {
            data.songs.MediaList.forEach(function(item, index) {
              // console.log(item);
              self.store.push({
                data: {
                  id: item.PersistentID, 
                  type: 'songs', 
                  attributes: item
                }
              });
            });
          }
        }

        return {
          'songs' : self.store.findAll('songs'), 
          'speakers' : self.store.findAll('speakers'),
          'beacon' : self.store.findAll('beacon')
        };

      });
    };

    var failSession = function()
    {
      console.log('failSession');
    };
    
    return this.get('hwhub').getSession(true).then(gotSession, failSession);
  },

  actions: {
    checkBluetooth: function()
    {
      this.get('beacon').isBluetoothEnabled();
    }
  }
});
