import DS from 'ember-data';
import Ember from 'ember';

export default DS.Model.extend({
  name : DS.attr(),
  uuid : DS.attr(),
  major : DS.attr(),
  minor : DS.attr(),
  flag : DS.attr(),
  status: DS.attr(),
  deviceid: DS.attr(),
  devicename: DS.attr(),
});
