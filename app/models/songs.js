import DS from 'ember-data';

export default DS.Model.extend({
  PersistentID : DS.attr(),
  Title : DS.attr(),
  Artist : DS.attr(),
  Duration : DS.attr(),
  AlbumTitle : DS.attr(),
});
