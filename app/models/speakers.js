import DS from 'ember-data';

export default DS.Model.extend({
  GroupName : DS.attr(),
  DeviceID : DS.attr(),
  IsPlaying : DS.attr(),
  Volume : DS.attr(),
  ModelName : DS.attr(),
  DeviceName : DS.attr(),
  Active : DS.attr()
});
