import Ember from 'ember';

export function initialize(instance) {
  let beaconService = instance.container.lookup('service:beacon');

  if(typeof cordova !== 'undefined') {
    document.addEventListener('deviceready', function() {
      console.log('beacon:ready');
      beaconService.isBluetoothEnabled();
      beaconService.isBluetoothAuthorised();

      Ember.RSVP.hash({
        btOn: beaconService.isBluetoothOn,
        locationAuth: beaconService.isBeaconAuthorised
      }).then(function(){
        console.log('inz / beacon');
        beaconService.startMonitoringAndRanging();
      });      
    }, false);

    document.addEventListener('resume', function() {
      console.log('beacon:resume');
      beaconService.isBluetoothEnabled();
      beaconService.isBluetoothAuthorised();
    }, false);

  }
}

export default {
  name: 'beacon',
  initialize: initialize
};
