export function initialize(instance) {
  let store = instance.container.lookup('service:store');

  // console.log('service/data');

  /*store.push({
    data: [{
      id: '1',
      type: 'beacon',
      attributes : {
        uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
        major: 28622,
        minor: 7710,
        name: 'M11',
        flag: false,
        deviceid: '57247907002544',
      }
    },
    {
      id: '2',
      type: 'beacon',
      attributes : {
        uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
        major: 28622,
        minor: 60040,
        name: 'M12',
        flag: false,
        deviceid: '142537350789296',
      }
    }]
  });*/

  store.push({
    data: [{
      id: '1',
      type: 'beacon',
      attributes : {
        uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
        major: 28622,
        minor: 60000,
        name: 'M11',
        flag: false,
      }
    },
    {
      id: '2',
      type: 'beacon',
      attributes : {
        uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
        major: 46014,
        minor: 33998,
        name: 'M12',
        flag: false,
      }
    }]
  });
}

export default {
  name: 'data',
  initialize: initialize
};

