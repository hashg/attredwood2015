import Ember from 'ember';

export function initialize(instance) {
  // let hwhubService = instance.container.lookup('service:hwhub');
  // hwhubService.initialize(); 
  if(typeof cordova !== 'undefined') {
    document.addEventListener('deviceready', function() {
      console.log('hwhub:ready');
    }, false);
  }
}

export default {
  name: 'hwhub',
  initialize: initialize
};
