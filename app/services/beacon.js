import Ember from 'ember';

export default Ember.Service.extend({

  // History of enter/exit events.
  store: Ember.inject.service(),
  mBeacon : null,
  isBluetoothOn : false,
  // isBeaconAuthorised : false,

  isBluetoothEnabled: function() {
    // console.log('isBluetoothEnabled');
    var self = this;

    // Request permission from user to access location info.
    cordova.plugins.locationManager.requestAlwaysAuthorization();
    
    function isBluetoothEnabledSuccess(result)
    {
      // console.log('isBluetoothEnabledSuccess');
      // console.log(result);
      self.set('isBluetoothOn', result);
    }

    function isBluetoothEnabledFailed(error)
    {
      // console.log('isBluetoothEnabledFailed');
      // console.log(error);
      self.set('isBluetoothOn', false);
    }

    cordova.plugins.locationManager.isBluetoothEnabled()
      .then(isBluetoothEnabledSuccess, isBluetoothEnabledFailed)
      .fail(console.error)
      .done();
  },

  isBluetoothAuthorised: function() {
    // console.log('isBluetoothAuthorised');

    // Request permission from user to access location info.
    cordova.plugins.locationManager.requestAlwaysAuthorization();

    var self = this;
    function getAuthSuccess(result)
    {
      // console.log('getAuthSuccess');
      // console.log(result);

      /*
      // Emitter uses bluetooth for your location to find nearby beacons. 
      // To enable, go to Settings > Privacy > Location Services > {{app}} and Tap on Always.
      */

      /*
      authorizationStatus: "AuthorizationStatusDenied"
      authorizationStatus: "AuthorizationStatusAuthorizedWhenInUse"
      authorizationStatus: "AuthorizationStatusAuthorized"
      */
      // console.log(result.authorizationStatus);
      switch(result.authorizationStatus)
      {
        case "AuthorizationStatusDenied":
          self.set('isBeaconAuthorised', false);
          break;
          // return false;
        case "AuthorizationStatusAuthorizedWhenInUse":
        case "AuthorizationStatusAuthorized":
          self.set('isBeaconAuthorised', true);
          break;
          // return  true;
        default:
          self.set('isBeaconAuthorised', false);
          break;
          // return false;
      }
    }

    function getAuthFail(error)
    {
      // console.log('getAuthFail');
      // console.log(error);
      self.set('isBeaconAuthorised', false);
      // return false;
    }

    cordova.plugins.locationManager.getAuthorizationStatus()
      .then(getAuthSuccess, getAuthFail)
      .fail(console.error)
      .done();
  },

// History of enter/exit events.
  mRegionEvents : [],


  // Nearest ranged beacon.
  mNearestBeacon : null,

  //all beacons
  mBeacons : null,

  //all beacon regions
  mBeaconRegions : [],

  // Timer that displays nearby beacons.
  mNearestBeaconDisplayTimer : null,

  // Background flag.
  mAppInBackground : false,

  // Background notification id counter.
  mNotificationId : 0,

  // Mapping of region event state names.
  // These are used in the event display string.
  mRegionStateNames :
  {
    'CLRegionStateInside': 'Enter',
    'CLRegionStateOutside': 'Exit'
  },

  // Here monitored regions are defined.
  // TODO: Update with uuid/major/minor for your beacons.
  // You can add as many beacons as you want to use.
  /*mRegions :
  [
    {
      uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
      major: 28622,
      minor: 7710,
      name: 'M31',
      id: '1'
    },
    {
      uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
      major: 28622,
      minor: 60040,
      name: 'M32',
      id: '2'
    }
  ],*/

  mRegions : 
  [
    {
      uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
      major: 28622,
      minor: 60000,
      name: 'M11',
      id: '1'
    },
    {
      uuid: "B9407F30-F5F8-466E-AFF9-25556B57FE6D",
      major: 46014,
      minor: 33998,
      name: 'M12',
      id: '2'
    }
  ],

  startNearestBeaconDisplayTimer: function()
  {
    console.log('startNearestBeaconDisplayTime');
    this.set('mNearestBeaconDisplayTimer', setInterval(this.displayNearestBeacon(), 1000));
  },

  stopNearestBeaconDisplayTimer : function()
  {
    console.log('stopNearestBeaconDisplayTimer');
    clearInterval(this.get('mNearestBeaconDisplayTimer'));
    this.set('mNearestBeaconDisplayTimer', null);
  },

  startMonitoringAndRanging : function()
  {
    console.log('startMonitoringAndRanging');
    var self = this;

    document.addEventListener('pause', onAppToBackground, false);
    document.addEventListener('resume', onAppToForeground, false);

    function onAppToBackground()
    {
      self.set('mAppInBackground' , true);
      self.stopNearestBeaconDisplayTimer();
      console.log('onAppToBackground');
    }

    function onAppToForeground()
    {
      self.set('mAppInBackground' , false);
      self.startNearestBeaconDisplayTimer();
      // self.displayRegionEvents();
      console.log('onAppToForeground');
    }

    function onDidDetermineStateForRegion(result)
    {
      // self.saveRegionEvent(result.state, result.region.identifier);
      // self.displayRecentRegionEvent();
    }

    function onDidRangeBeaconsInRegion(result)
    {
      // console.log('onDidRangeBeaconsInRegion');
      // console.log(result);
      self.updateNearestBeacon(result.beacons);
    }

    function onError(errorMessage)
    {
      console.log('Monitoring beacons did fail: ' + errorMessage);
    }

    // Request permission from user to access location info.
    cordova.plugins.locationManager.requestAlwaysAuthorization();

    // Create delegate object that holds beacon callback functions.
    var delegate = new cordova.plugins.locationManager.Delegate();
    cordova.plugins.locationManager.setDelegate(delegate);

    // Set delegate functions.
    delegate.didDetermineStateForRegion = onDidDetermineStateForRegion;
    delegate.didRangeBeaconsInRegion = onDidRangeBeaconsInRegion;

    // Start monitoring and ranging beacons.
    self.startMonitoringAndRangingRegions(self.get('mRegions'), onError);

    self.startNearestBeaconDisplayTimer();
    // self.displayRegionEvents();
  },

  startMonitoringAndRangingRegions : function(regions, errorCallback)
  {
    console.log('startMonitoringAndRangingRegions');
    // console.log(regions);
    // Start monitoring and ranging regions.
    for (var i in regions)
    {
      // console.log(i);
      if(regions[i].id)
      {
        this.startMonitoringAndRangingRegion(regions[i], errorCallback);
      }
    }
    // console.log(this.get('mBeaconRegions'));
  },

  startMonitoringAndRangingRegion : function(region, errorCallback)
  {
    console.log('startMonitoringAndRangingRegion');
    // console.log(region);
    // Create a region object.
    var beaconRegion = new cordova.plugins.locationManager.BeaconRegion(
      region.id,
      region.uuid,
      region.major,
      region.minor);

    this.get('mBeaconRegions').push(beaconRegion);

    // Start ranging.
    cordova.plugins.locationManager.startRangingBeaconsInRegion(beaconRegion)
      .fail(errorCallback)
      .done();

    // Start monitoring.
    cordova.plugins.locationManager.startMonitoringForRegion(beaconRegion)
      .fail(errorCallback)
      .done();
  },

  stopMonitoringAndRangingRegions : function()
  {
    console.log('stopMonitoringAndRangingRegions');
    var regions = this.get('mBeaconRegions');

    //stop monitoring and ranging regions.
    for (var i in regions)
    {
      this.stopMonitoringAndRangingRegion(regions[i]);
    }

    //clean up the mBeaconRegions.
    this.set('mBeaconRegions', []);

    //clear mNearestBeacon
    // this.set('mNearestBeacon', null);
  },

  stopMonitoringAndRangingRegion : function(region)
  {
    console.log('stopMonitoringAndRangingRegion');

    // stop ranging.
    cordova.plugins.locationManager.stopRangingBeaconsInRegion(region)
      .fail(errorCallback)
      .done();

    // stop monitoring.
    cordova.plugins.locationManager.stopMonitoringForRegion(region)
      .fail(errorCallback)
      .done();
  },

  saveRegionEvent : function(eventType, regionId)
  {
    console.log('saveRegionEvent');
    var self = this;
    // Save event.
    self.get('mRegionEvents').addObject(
    {
      type: eventType,
      time: self.getTimeNow(),
      regionId: regionId
    });

    // Truncate if more than ten entries.
    if (self.get('mRegionEvents').length > 10)
    {
      self.get('mRegionEvents').shiftObject();
    }
  },

  getBeaconId : function(beacon)
  {
    return beacon.uuid + ':' + beacon.major + ':' + beacon.minor;
  },

  isSameBeacon : function(beacon1, beacon2)
  {
    return this.getBeaconId(beacon1) === this.getBeaconId(beacon2);
  },

  isNearerThan : function(beacon1, beacon2)
  {
    // console.log(beacon1.accuracy +":"+ beacon2.accuracy);
    return beacon1.accuracy > 0
      && beacon2.accuracy > 0
      && beacon1.accuracy < beacon2.accuracy;
  },

  updateNearestBeacon : function(beacons)
  {
    // console.log('updateNearestBeacon');
    // console.log(beacons);

    for (var i = 0; i < beacons.length; ++i)
    {
      var beacon = beacons[i];
      var _mNearestBeacon = this.get('mNearestBeacon');
      
      if (!_mNearestBeacon)
      {
        this.set('mNearestBeacon' , beacon);
      }
      else
      {
        if (this.isSameBeacon(beacon, _mNearestBeacon) ||
          this.isNearerThan(beacon, _mNearestBeacon))
        {
          this.set('mNearestBeacon' , beacon);
        }
        else if(_mNearestBeacon.accuracy < 0)
        {
          this.set('mNearestBeacon' , beacon);
        }
      }
    }
    // console.log(this.get('mNearestBeacon'));
  },

  displayNearestBeacon : function()
  {
    console.log('displayNearestBeacon');
    var self = this;
    var _mNearestBeacon = self.get('mNearestBeacon');
    if( _mNearestBeacon )
    {
    console.log('UUID: ' + _mNearestBeacon.uuid + '\n'
      + 'Major: ' + _mNearestBeacon.major + '\n'
      + 'Minor: ' + _mNearestBeacon.minor + '\n'
      + 'Proximity: ' + _mNearestBeacon.proximity + '\n'
      + 'Distance: ' + _mNearestBeacon.accuracy + '\n'
      + 'RSSI: ' + _mNearestBeacon.rssi + '\n'
      );
    }
  },

  displayRecentRegionEvent : function()
  {
    console.log('displayRecentRegionEvent');
    var self = this;
    var event = self.get('mRegionEvents')[self.get('mRegionEvents').length - 1];
    var notificationData = self.get('mRegions')[event.regionId];

    if (!event) { return; }
    // console.log(event);
    if(event.type === "CLRegionStateInside")
    {
      if (self.get('mAppInBackground'))
      {
        // Create notification.
        cordova.plugins.notification.local.schedule({
          id: ++self.mNotificationId,
          title: notificationData["name"]+" : "+notificationData["notification"],
          // sound: 'file://beep.caf',
          data: notificationData 
        });
      }
    }
  },

  displayRegionEvents : function()
  {
    console.log('displayRegionEvents');
    var self = this;
    var element = [];
    // Clear list.

    // Update list.
    for (var i = self.mRegionEvents.length - 1; i >= 0; --i)
    {
      var event = self.mRegionEvents[i];
      var title = self.getEventDisplayString(event);
      element.addObject(title);
      
    }

    // If the list is empty display a help text.
    if (self.mRegionEvents.length <= 0)
    {
      element.addObject('Waiting for region events, please move into or out of a beacon region.');
    }

    // console.log(element);
  },

  getEventDisplayString : function(event)
  {
    console.log('getEventDisplayString');
    var self = this;
    return event.time + ': '
      + self.mRegionStateNames[event.type] ;
  },

  getTimeNow : function()
  {
    console.log('getTimeNow');
    function pad(n)
    {
      return (n < 10) ? '0' + n : n;
    }

    function format(h, m, s)
    {
      return pad(h) + ':' + pad(m)  + ':' + pad(s);
    }

    var d = new Date();
    return format(d.getHours(), d.getMinutes(), d.getSeconds());
  }


});
