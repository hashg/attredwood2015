import Ember from 'ember';

export default Ember.Service.extend({
  data: Ember.inject.service(),
  namespace : 'hkfollowme',

  // server: "192.168.1.84",
  // port : '8080',
  // version : "v1",
  timeout : 5000,
  session: null,
  playing: null,


  url : Ember.computed('server', 'port', 'version', function() {
    var server = this.get('server');
    var port = this.get('port');
    var uri = port ? server + ':' + port : server ;
    var version = this.get('version');
    return "http://" + uri + "/" + version ;
  }),

  buildURL : function(method, params) {
    var url = this.get('url');
    var session = this.get('session');
    var query = null;
    if( params )
    {
      query = '&' + params.join('&');
    }

    if(method === "init_session")
    {
      var query = params.join('&');
      return url + "/" + method + "?" + query;
    }
    if(query)
    {
      return url + "/" + method + "?SessionID=" + session + query;
    }
    else
    {
      return url + "/" + method + "?SessionID=" + session;
    }
  },

  initialize: function() {
    console.log('HWHUB Init');
    this.getSession();
  },

  getSession : function(returnPromise) {
    console.log('HWHUB Session');
    var self = this;
    var method = "init_session";
    var url = this.buildURL(method , ["Priority=1000"]);
    // console.log(url);
    if( returnPromise )
    {
      return ajax(url, {}, this.get('timeout'));
    }
    else
    {
      ajax(url, {}, this.get('timeout')).then(
        function(data) {
          self.set('session', data.SessionID);
          console.log('init_session' + JSON.stringify(data));
        },
        function(error) {
          console.log('init_session failed');
          self.set('session', "failed");
      });
    }
    
  },

  command : function(method, params ) {
    console.log('HWHUB ' + method);
    var self = this;
    var url = this.buildURL(method , params);
    console.log(url);

    ajax(url, {}, this.get('timeout')).then(
      function(data) {
        console.log(method + ": Success");
      },
      function(error) {
        console.log(method + ": Failed");
    });
  },

  play: function(song_id) {
    this.command("play_hub_media", ["PersistentID=" + song_id]);
  },

  playParty: function(song_id, promise) {
    if( promise )
    {
      return this.command_promise("play_hub_media_party_mode", ["PersistentID=" + song_id]);
    }
    this.command("play_hub_media_party_mode", ["PersistentID=" + song_id]);
  },

  playDevice: function(song_id, device_id) {
    this.command("play_hub_media_selected_speakers", ["PersistentID=" + song_id, "DeviceIDList="+ device_id]);
  },

  pause: function() {
    this.command("pause_play");
  },

  resume: function(song_id) {
    this.command("resume_hub_media", ["PersistentID=" + song_id]);
  },

  resumeParty: function(song_id) {
    this.command("resume_hub_media_party_mode", ["PersistentID=" + song_id]);
  },

  resumeDevice: function(song_id, device_id) {
    this.command("resume_hub_media_selected_speakers", ["PersistentID=" + song_id, "DeviceIDList="+ device_id]);
  },

  stop: function() {
    this.command("stop_play");
  },

  addSpeaker: function(device_id)
  {
    this.command("add_device_to_session", ["DeviceID=" + device_id]);
  },

  removeSpeaker: function(device_id)
  {
    this.command("remove_device_from_session", ["DeviceID=" + device_id]); 
  },

  setVolumeParty : function(level) {
    this.command("set_volume", ["Volume=" + level]); 
  },

  setVolumeDevice : function(level, device_id, promise) {
    if ( promise )
    {
      return this.command_promise("set_volume_device", ["DeviceID=" + device_id, "Volume=" + level]);
    }
    this.command("set_volume_device", ["DeviceID=" + device_id, "Volume=" + level]);
  },

  command_promise : function(method, params) {
    console.log('HWHUB ' + method);
    var self = this;
    var url = this.buildURL(method, params);
    console.log(url);

    return ajax(url, {}, this.get('timeout'));
  },

  speakers_list : function(device_id)
  {
    return this.command_promise("device_list");
  },

  songs_list : function()
  {
    return this.command_promise("media_list");
  },

  getVolumeDevice : function(device_id) {
    return this.command_promise("get_volume_device", ["DeviceID=" + device_id]);
  },


/*
  removeSpeakerAllExcept : function(device_id)
  {
    var self = this;
    Ember.$.ajax({
      url: self.get('server') + "device_list?SessionID=" + self.get('session'),
      type: "GET",
      success: function(data) {
        // console.log('removeSpeakerAll' + data);
        if( data && data.DeviceList && data.DeviceList.length > 1)
        {
          for (var i = data.DeviceList.length - 1; i >= 0; i--) {
            console.log(data.DeviceList[i].DeviceID);
            var dev = data.DeviceList[i].DeviceID;
            if(device_id != dev)
            {
              self.removeSpeaker(data.DeviceList[i].DeviceID);
            }
          };
        }
      },
      error: function(error) {
        debugger;
        console.log('removeSpeakerAll failed');
      }
    });
  }, 

  removeSpeakerAll : function()
  {
    var self = this;
    Ember.$.ajax({
      url: self.get('server') + "device_list?SessionID=" + self.get('session'),
      type: "GET",
      success: function(data) {
        console.log('removeSpeakerAll' + data);
        if( data && data.DeviceList && data.DeviceList.length > 1)
        {
          for (var i = data.DeviceList.length - 1; i >= 0; i--) {
            console.log(data.DeviceList[i].DeviceID);
            self.removeSpeaker(data.DeviceList[i].DeviceID);
          };
        }
      },
      error: function(error) {
        debugger;
        console.log('removeSpeakerAll failed');
      }
    });
  },

*/




    // http://192.168.1.10:8080/v1/init_session?Priority=100;
    // {"SessionID" : "1000"}

/*
    http://192.168.1.10:8080/v1/device_list?SessionID=1000
    {"DeviceList":
      [{"GroupName":"Bathroom",
      "Role":21,
      "MacAddress":"b0:38:29:1b:36:1f",
      "WifiSignalStrength":-47,
      "Port":44055,
      "Active":true,
      "DeviceName":"Adapt1",
      "Version":"0.1.6.2",
      "ModelName":"Omni Adapt",
      "IPAddress":"192.168.1.40",
      "GroupID":"3431724438",
      "Volume":47,
      "IsPlaying":false,
      "DeviceID":"34317244381360"
      },
      {"GroupName":"Temp",
      "Role":21,
      "MacAddress":"b0:38:29:1b:9e:75",
      "WifiSignalStrength":-53,
      "Port":44055,
      "Active":true,
      "DeviceName":"Adapt",
      "Version":"0.1.6.2",
      "ModelName":"Omni Adapt",
      "IPAddress":"192.168.1.39",
      "GroupID":"1293219209",
      "Volume":47,
      "IsPlaying":false,
      "DeviceID":"129321920968880"
      }]
    }
*/
  
/*  
    http://192.168.1.10:8080/v1/media_list?SessionID=1000
    {"MediaList": [
      {"PersistentID":"7387446959931482519",
      "Title":"I Will Run To You",
      "Artist":"Hillsong",
      "Duration":436,
      "AlbumTitle":"Simply Worship"
      },
      {"PersistentID":"5829171347867182746",
      "Title":"I'm Yours [ORIGINAL DEMO]",
      "Artist":"Jason Mraz",
      "Duration":257,
      "AlbumTitle":"Wordplay [SINGLE EP]"}
    ]}
*/

/*
    http://192.168.1.10:8080/v1/play_hub_media?SessionID=1000&PersistentID=7387446959931482519
    {"Result":"true"}
*/
/*
    // Play a song in the Media list with selected speakers
    http://192.168.1.10:8080/v1/play_hub_media_selected_speakers?SessionID=1000&PersistentID=7387446959931482519&DeviceIDList=34317244381360,129321920968880
    {"Result":"true"}
*/

/*
    Resume the Current Playback with Hub Media
    Request: http://192.168.1.10:8080/v1/resume_hub_media?SessionID=1000&PersistentID=7387446959931482519
    {"Result":"true"}
*/

/*
    http://192.168.1.10:8080/v1/resume_hub_media_selected_speakers?SessionID=1000&PersistentID=7387446959931482519&DeviceIDList=34317244381360,129321920968880
    {"Result":"true"}
*/


});



function ajax(url, headers, timeout, options) {
  // Ember.Logger.debug('hwhub/ajax');
  return new Ember.RSVP.Promise(function(resolve,reject){
    var options = options || {};

    options.headers = headers;
    options.timeout = timeout;

    options.success = function(data, textStatus, xhr) {
      resolve(data, textStatus, xhr);
    };

    options.error = function(jqXHR, status, error) {
      reject(jqXHR, status, error);
    };

    Ember.$.ajax(url, options);
  });
}
